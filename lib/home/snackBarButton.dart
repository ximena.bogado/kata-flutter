import 'package:flutter/material.dart';


final snackBar = SnackBar(
  content: const Text('Estamos en construccion'),
  action: SnackBarAction(
    label: 'Cerrar',
    onPressed: () {
    },
  ),
);

class SnackBarButton  extends StatelessWidget {

  final IconData icon;
  final String text;

  const SnackBarButton(this.icon, this.text, {super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      },
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 5),
        decoration: const BoxDecoration(
          color: Colors.grey,
        ),
        child: Row(
          children: [
            Icon(
                icon
            ),
            Text(text)
          ],
        ),
      ),
    );
  }
}