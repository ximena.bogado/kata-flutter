import 'package:flutter/material.dart';

Widget profileName(String name) => Container(
    padding: const EdgeInsets.only(right: 8, bottom: 8),
    child: Row(
      children: [
        Text(
          name,
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
        Icon(
          Icons.circle,
          color: Colors.green,
          size: 15,
        ),
      ],
    )
);