import 'package:flutter/material.dart';
import 'package:prueba_flutter/home/profileName.dart';
import 'package:prueba_flutter/home/profileTitle.dart';

Widget dataProfile = Column(
  crossAxisAlignment: CrossAxisAlignment.start,
  children: const [
    Text(
        'Executive Director,'
    ),
    Text(
      'Bank Resource',
    ),
    Text(
      'Management',
    )
  ],
);

Widget profile(name) => Column(
  crossAxisAlignment: CrossAxisAlignment.start,
  children: [
    profileTitle,
    profileName(name),
    dataProfile,
  ],
);