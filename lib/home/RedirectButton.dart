import 'package:flutter/material.dart';
import '../editProfile/EditProfilePage.dart';

class RedirectButton  extends StatelessWidget {

  final IconData icon;
  final String text;
  final Function rename;

  const RedirectButton(this.icon, this.text, this.rename, {super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 5),
        decoration: const BoxDecoration(
          color: Colors.grey,
        ),
        child: Row(
          children: [
            Icon(
                icon
            ),
            Text(text),
          ],
        ),
      ),
      onTap: () {
        Navigator.push(
            context, 
            MaterialPageRoute(builder: (context) {
              return EditProfilePage(rename);
            }),
        );
      }
    );
  }
}
