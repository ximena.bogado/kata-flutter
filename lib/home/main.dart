import 'package:flutter/material.dart';
import 'package:prueba_flutter/editProfile/EditProfilePage.dart';
import 'package:prueba_flutter/home/titleSection.dart';
import 'package:prueba_flutter/home/buttons.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/': (context) => Home(),
      }
    );
  }
}

class Home extends StatelessWidget {
  const Home({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Bienvenido a la App'),
      ),
      body: Profile(),
    );
  }
}

class Profile extends StatefulWidget {
  const Profile({
    Key? key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  String _name = "Mariam";

  void _rename( String name ) {
    setState(() {
      _name = name;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(flex: 3, child: titleSection(_name)),
        Container(
          padding: const EdgeInsets.symmetric(vertical: 5),
          color: Colors.grey,
          child: Row(children: [Text('Timezone: ${DateTime.now().timeZoneName}')]),
        ),
        Expanded(child: buttons(_rename)),
        Container(
          margin: const EdgeInsets.only(bottom: 5),
          child:
          const Text('Powered by 10pines',
            style: TextStyle(
              fontSize: 14,
              fontStyle: FontStyle.italic,
            ),
          ),
        ),
      ],
    );
}}