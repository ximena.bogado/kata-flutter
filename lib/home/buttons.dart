import 'package:flutter/material.dart';
import 'package:prueba_flutter/home/RedirectButton.dart';
import 'package:prueba_flutter/home/snackBarButton.dart';
import 'package:prueba_flutter/home/RedirectPageButton.dart';

Widget buttons(Function rename ) => Container(
    margin: const EdgeInsets.symmetric(vertical: 4),
    child: ListView(
      children: [
        const SnackBarButton(Icons.build, 'Open preferences'),
        RedirectButton(Icons.person, 'Edit your profile', rename),
        const RedirectPageButton(Icons.local_post_office, 'Posts'),
      ],
    )
);