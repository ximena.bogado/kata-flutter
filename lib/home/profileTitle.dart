import 'package:flutter/material.dart';

Widget profileTitle = Container(
  padding: const EdgeInsets.only(bottom: 8),
  child: const Text(
      'Profile',
      style: TextStyle(
        fontWeight: FontWeight.bold,
      )
  ),
);