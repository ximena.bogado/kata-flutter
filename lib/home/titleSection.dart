import 'package:flutter/material.dart';
import 'package:prueba_flutter/home/profileData.dart';

Widget titleSection(name) => Container(
  padding: const EdgeInsets.all(32),
  decoration: const BoxDecoration(
      image: DecorationImage(image: AssetImage('assets/background.png'), fit: BoxFit.fill)
  ),
  child: profile(name),
);