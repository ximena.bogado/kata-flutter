import 'package:flutter/material.dart';
import '../posts/OtherPage.dart';

class RedirectPageButton  extends StatelessWidget {

  final IconData icon;
  final String text;

  const RedirectPageButton(this.icon, this.text, {super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 5),
          decoration: const BoxDecoration(
            color: Colors.grey,
          ),
          child: Row(
            children: [
              Icon(
                  icon
              ),
              Text(text),
            ],
          ),
        ),
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) {
              return OtherPage();
            }),
          );
        }
    );
  }
}

class AddButton extends StatelessWidget {
  final Function addFriend;

  const AddButton(this.addFriend, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
        onPressed: () => addFriend('Pepito'),
        child: Icon(Icons.add)
    );
  }
}

