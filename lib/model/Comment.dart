class Comment {
  final int id;
  final String name;
  final String body;
  final int postId;
  final String email;

  Comment({
    required this.id,
    required this.name,
    required this.body,
    required this.postId,
    required this.email,
  });

  factory Comment.fromJson(Map<String, dynamic> json) {
    return Comment(
        id: json['id'],
        name: json['name'],
        body: json['body'],
        postId: json['postId'],
        email: json['email']
    );
  }
}