import 'package:flutter/material.dart';

class EditProfileForm extends StatefulWidget {
  final Function rename;

  const EditProfileForm(this.rename, { Key? key,}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _EditProfileFormState(rename);
}

class _EditProfileFormState extends State<EditProfileForm>{
  final Function rename;
  String _newName = '';

  _EditProfileFormState(this.rename);

  void _setNewName(newName) {
    setState(() {
      _newName = newName;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        children: [
          TextField(
              onChanged: (text) => _setNewName(text)),
          TextButton(
              child: const Text('Rename'),
              onPressed: () {
                rename(_newName);
                Navigator.pop(context);
              }
          ),
        ]
    );
  }
}