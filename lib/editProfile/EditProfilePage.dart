import 'package:flutter/material.dart';
import 'package:prueba_flutter/editProfile/EditProfileForm.dart';

class EditProfilePage extends StatelessWidget {
  final Function rename;

  const EditProfilePage(this.rename, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Bienvenido a la App'),
      ),
      body: EditProfileForm(rename),
    );
  }
}