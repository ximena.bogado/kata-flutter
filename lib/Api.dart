import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:prueba_flutter/model/Post.dart';
import 'model/Comment.dart';

class Api {

  Future<List<Post>> getAllPost() async {
   return http
       .get(Uri.parse('https://jsonplaceholder.typicode.com/posts'))
       .then((value) => jsonToPosts(value))
       .catchError((error) => throw Exception(error.toString()));
  }

  Future<List<Comment>> getComments(int id) async {
    return http
        .get(Uri.parse('https://jsonplaceholder.typicode.com/posts/${id}/comments'))
        .then((value) => jsonToComments(value))
        .catchError((error) => throw Exception(error.toString()));
  }

  List<Post> jsonToPosts(http.Response value) {
    return jsonDecode(value.body).map<Post>( (post) => Post.fromJson(post) ).toList();
  }

  List<Comment> jsonToComments(http.Response value) {
    return jsonDecode(value.body).map<Comment>( (comment) => Comment.fromJson(comment) ).toList();
  }
}