import 'package:flutter/material.dart';
import 'package:prueba_flutter/model/Post.dart';
import 'package:prueba_flutter/posts/Posts.dart';
import 'package:prueba_flutter/posts/PostsTitle.dart';

class PostPage extends StatelessWidget {
  const PostPage({
    Key? key,
    required this.posts,
  }) : super(key: key);

  final List<Post> posts;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Posts'),
        ),
        body: Container(
          decoration: BoxDecoration(
              color: Colors.greenAccent
          ),
          child: Column(
              children: [
                PostsTitle(),
                /*AddButton(_addFriend),*/
                Expanded(
                    child: posts.length > 0 ? Posts(posts: posts) : Text('no hay post')
                ),
              ]
          ),
        )
    );
  }
}