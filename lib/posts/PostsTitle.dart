import 'package:flutter/material.dart';

class PostsTitle extends StatelessWidget {
  const PostsTitle({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        heightFactor: 2,
        child: Text(
          'Posts',
          style: TextStyle(fontSize: 18, color: Colors.blue, fontWeight: FontWeight.bold),
        )
    );
  }
}