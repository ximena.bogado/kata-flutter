import 'package:flutter/material.dart';
import 'package:prueba_flutter/Api.dart';
import 'package:prueba_flutter/posts/PostPage.dart';
import '../model/Post.dart';

class OtherPage extends StatefulWidget {
  const OtherPage({Key? key}) : super(key: key);

  @override
  State<OtherPage> createState() => OtherPageState();
}

class OtherPageState extends State<OtherPage>{

  late Future<List<Post>> posts;

  /*void _addFriend(String friend) {
    setState(() {
      posts.add(friend);
    });
  }*/

  @override
  void initState() {
    super.initState();
    posts = Api().getAllPost();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Post>>(
        future: Api().getAllPost(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return PostPage(posts: snapshot.data!);
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const CircularProgressIndicator();
        }
        );
  }
}