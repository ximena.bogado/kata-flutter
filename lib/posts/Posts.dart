import 'package:flutter/material.dart';
import 'package:prueba_flutter/Api.dart';
import '../model/Post.dart';
import '../model/Comment.dart';

class Posts extends StatelessWidget {
  const Posts({
    Key? key,
    required this.posts,
  }) : super(key: key);

  final List<Post> posts;

  @override
  Widget build(BuildContext context) {
    return ListView(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        children: posts.map((post) =>
            Card(
                margin: EdgeInsets.all(20),
                elevation: 1,
                shape: RoundedRectangleBorder(
                    side: BorderSide(
                      color: Colors.black12,
                    ),
                    borderRadius: const BorderRadius.all(Radius.circular(12))
                ),
                child: Column(
                  children: [
                    ListTile(
                      title: Padding(
                          child: Text(post.title),
                          padding: EdgeInsets.symmetric(vertical: 5)),
                      subtitle: Text(post.body),
                    ),
                    Center(
                      child:
                      TextButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) {
                              return CommentPage(post);
                            }),
                          );
                        },
                        child: Text('Comentarios'),
                      ),
                    )
                  ],
                )
            )
        ).toList()
    );
  }
}
class CommentPage extends StatefulWidget {
  final Post post;

  const CommentPage(this.post, {Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => CommentPageState(post);
}

class CommentPageState extends State<StatefulWidget>{
  var post;
  late Future<List<Comment>> comments;
  
  CommentPageState(this.post);
  
  @override
  void initState() {
    comments = Api().getComments(post.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Comments'),
        ),
        body: Column(
          children: [
            CommentsTitle(),
            Padding(
                padding: EdgeInsets.all(5),
                child: Text(
                    post.title,
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                )
            ),
            FutureBuilder<List<Comment>>(
              future: Api().getComments(post.id),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Comments(comments: snapshot.data!);
                } else if (snapshot.hasError) {
                  return Text('${snapshot.error}');
                }
                return const CircularProgressIndicator();
              }
          ),
          ],
      )
    );
  }
}

class Comments extends StatelessWidget {
  final List<Comment> comments;
  
  const Comments({Key? key, required this.comments}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(child: ListView(
      shrinkWrap: true,
        scrollDirection: Axis.vertical,
        children:
          comments.map((comment) =>
              CommentCard(comment)
          ).toList()
    ));
  }
}

class CommentCard extends StatelessWidget {
  Comment comment;

  CommentCard(this.comment, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 1,
        color: Colors.greenAccent,
        shape: RoundedRectangleBorder(
            side: BorderSide(
              color: Colors.black12,
            ),
            borderRadius: const BorderRadius.all(Radius.circular(12))
        ),
        child:
            Column(
              children: [
                Text('email: ${comment.email}'),
                Text('name: ${comment.name}'),
                Padding(
                  padding: EdgeInsets.only(top: 5) ,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: Colors.black26),
                      borderRadius: BorderRadius.circular(10)
                    ),
                      child: Text(comment.body)
                  ))
              ],
            )
    );
  }
}

class CommentsTitle extends StatelessWidget {
  const CommentsTitle({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        heightFactor: 2,
        child: Text(
          'Comments',
          style: TextStyle(fontSize: 18, color: Colors.blue, fontWeight: FontWeight.bold),
        )
    );
  }
}
